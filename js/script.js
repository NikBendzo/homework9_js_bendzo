"use strict"

function renderList(array, parent = document.body) {
    const ul = document.createElement('ul');
    parent.appendChild(ul);
    
    array.forEach(item => {
        const li = document.createElement('li');
        if (Array.isArray(item)) {
            // Якщо елемент - масив, викликаємо функцію рекурсивно
            renderList(item, li);
        } else {
            li.textContent = item;
            ul.appendChild(li);
        }
    });
}

setTimeout(() => {
    document.body.innerHTML = '';
}, 3000);

let seconds = 3;
const timerInterval = setInterval(() => {
    seconds--;
    console.log(seconds); // Можна використовувати це значення для відображення таймера на сторінці
    if (seconds === 0) {
        clearInterval(timerInterval); // Зупинити таймер, коли досягнута 0
    }
}, 1000);

const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
renderList(array);